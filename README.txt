###############

Arizona Space Grant Consortium Rocksat-C Code Hub

Authors: Joel Thibault, Cathy McIntosh

###############

Structure

|--lib
|  |--Sensors <-- example
|  |  |--docs
|  |  |--examples
|  |  |--src
|  |     |- Bar.c
|  |     |- Bar.h
|  |--SD <-- example
|  |  |- Foo.c
|  |  |- Foo.h
|--src
   |- main.c
|- platformio.ini <--Dont touch this!
|- .gitignore <--Dont touch this!
|- README.txt <--This file, please only add information that is relevant to entire project

###############

Environment Set-Up

This project was transferred from an Arduino file to CPP files using VSCode and platformio. Editing
code can also be done with other environments (such as Atom, Visual Studio, Eclipse, etc.) but 
to push code to a board (in our case, the Adafruit Feather M0) you will need Arduino and 
the Platformio extension to Atom or VSCode.

***TL;DR: EDITING -> any code editor, UPLOADING -> Platformio using either VSCode or Atom

The Big Boi Set-Up:
1. Install Arduino @ https://www.arduino.cc/en/Main/Software
2. Install either VSCode (preferred because of stability) or Atom
    VSCode @ https://code.visualstudio.com/download
    Atom @ https://atom.io/
3. Install Platformio extension for your IDE @ http://docs.platformio.org/en/latest/ide/pioide.html
    *If there's any problems at this point, contact Cathy or Joel and we'll help

Libary Installations:
4. In Platformio, go to Libraries and search for these libraries and install them:
    - Adafruit 9DOF Library by Adafruit Industries
    - Adafruit ADS1X15 by Adafruit
    - Adafruit BME280 Library by Adafruit
    - Adafruit CCS811 Library by Adafruit
    - Adafruit GPS Library by Adafruit # only when we have GPS capabilities on board
    - Adafruit L3GD20 U by Adafruit
    - Adafruit LSM303DLHC by Adafruit
    - Adafruit LSM9DS1 Library by Adafruit
    - Adafruit MCP9808 Library by Adafruit
    - Adafruit SGP30 Sensor by Adafruit
    - Adafruit Unified Sensor by Adafruit
    - SD by Arduino

Git Set-Up:
5. To clone the remote repo to your local machine, install Git @ https://git-scm.com/downloads. You should
have Git Bash, Git CMD, and Git GUI. We will use Git Bash.
6. In Git Bash, change the directory to the local folder you want the 'rocksatc-2018' folder to be
in. TIP: don't make a rocksatc-2018 folder and clone there, you will nest folders
7. Type the command 'git clone https://gitlab.com/crmcintosh/rocksatc-2018.git' and cd into that folder
    *If you are rejected, you might not have the rights to this repo- talk to Cathy so she can add you!
8. To go from Git Bash to VSCode, you type 'code .' and the IDE will open with the rocksatc folder.

Uploading to the Boi:
9. Once connected to the board, check your device manager that the Feather is recognized on your computer
and check the devices on Platformio. Using the Platformio extension, click 'Upload' and your program will
go to the board.

###############

Coding Etiquette

Do's:
    - Comment before blocks of code briefly explaining what the purpose of the code is and
    include comments at least before each function
    - Create a branch each time you want to make a change or work - this way master stays clean
    - Ask for help if you need it and add resources you think are useful below

Don't:
    - Comment out blocks of code without briefly explaining why it isn't used (ex. '//Sensor not on board yet',
    '//Needs more information to finish')
    - Delete other's code without telling them/changing code without a detailed commit message

###############

Useful Links and Resources

Git Resources:
    Basic Git commands -> https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html
    Intro to Version Control -> https://betterexplained.com/articles/a-visual-guide-to-version-control/
    Common Linux Commands -> http://www.dummies.com/computers/operating-systems/linux/common-linux-commands/

C/C++ Resources:
    C++ Libarary -> http://www.cplusplus.com/doc/tutorial/
    Review and Tutorials of C and C++ -> https://www.cprogramming.com/tutorial.html

Arduino Basics:
    Summary of different Arduino boards and capabilities -> https://learn.sparkfun.com/tutorials/what-is-an-arduino
    Adafruit Feather M0 Datalogger Datasheet -> https://cdn-learn.adafruit.com/downloads/pdf/adafruit-feather-m0-adalogger.pdf
    What's I2C and how are we communicating with the sensors -> https://learn.sparkfun.com/tutorials/i2c
    What's SPI -> https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi?_ga=2.24094578.910200932.1521155396-1772722796.1509557037

