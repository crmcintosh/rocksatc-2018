#include <Arduino.h>
#define mySerial Serial1
#include <Adafruit_GPS.h> //not using GPS on board yet
#define Serial SERIAL_PORT_USBVIRTUAL
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>  //BME 280
#include "Adafruit_MCP9808.h" //tempsensor
#include <Adafruit_LSM9DS1.h> //9dof
#include <Adafruit_CCS811.h>  //CCS811
#include "Adafruit_SGP30.h"   //SGP30
#include <Adafruit_ADS1015.h> //ADC

//Define SeaLevel
#define SEALEVELPRESSURE_HPA (1013.47)

//Define Sensores
Adafruit_GPS GPS(&Serial1); //Not using GPS on board yet

Adafruit_BME280 bmeA; // I2C
Adafruit_BME280 bmeB; // I2C
Adafruit_LSM9DS1 lsm = Adafruit_LSM9DS1();
Adafruit_MCP9808 MCP_A = Adafruit_MCP9808(); //Temp Sensor
Adafruit_MCP9808 MCP_B = Adafruit_MCP9808(); //Temp Sensor
Adafruit_CCS811 ccs;                         //Gas sensor
Adafruit_SGP30 sgp;                          //Gas sensor
Adafruit_ADS1115 ads1115;

//set Delay Between Data Points
int ColectDelay = 848;

//FileName and 0 indexs
char Filename[] = "FEB000.csv";
int first0Index = 3;
int second0Index = 4;
int third0Index = 5;

//Define pins for leds and SD chipslect
int SD_chipSelect = 4;
int greenLED = 13;
//int redLED = 10;

//Baseline readings for the SGP gas sensor in hexidecimal
uint16_t TVOC_base, eCO2_base;

//ADXL277 pins
const int xInput = A0;
const int yInput = A1;
const int zInput = A2;
const int sampleSize = 10;

// Raw Ranges:
int xRawMin = 512;
int xRawMax = 520;

int yRawMin = 512;
int yRawMax = 519;

int zRawMin = 512;
int zRawMax = 519;

int main()
{
    Serial.begin(115200); // this baud rate doesn't actually matter!
    mySerial.begin(9600);
    delay(2000);

    //Start GPS
    // you can send various commands to get it started
    mySerial.println(PGCMD_NOANTENNA);
    delay(100);
    // mySerial.println(PMTK_AIC_ON); //Not in Adafruit_GPS.h - need to find definition or replace with more current AIC command
    //delay(100);
    mySerial.println(PMTK_SET_NMEA_OUTPUT_RMCGGA);
    delay(100);
    mySerial.println(PMTK_SET_NMEA_UPDATE_10HZ);

    pinMode(greenLED, OUTPUT);
    //  pinMode(redLED, OUTPUT);
    digitalWrite(greenLED, HIGH);
    //  digitalWrite(redLED, HIGH);

    // 9DOF setup

    if (!lsm.begin())
    {
        Serial.println("Oops ... unable to initialize the LSM9DS1. Check your wiring!");
        while (1)
            ;
    }
    else
    {
        lsm.setupAccel(lsm.LSM9DS1_ACCELRANGE_16G);
        lsm.setupMag(lsm.LSM9DS1_MAGGAIN_8GAUSS);
        lsm.setupGyro(lsm.LSM9DS1_GYROSCALE_2000DPS);
    }

    if (!bmeA.begin())
    { //default BME
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
        //digitalWrite(redLED, HIGH);
        //while (1);
    }
    if (!bmeB.begin(0x76))
    { //second BME
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
        //digitalWrite(redLED, HIGH);
        //while (1);
    }

    if (!MCP_A.begin(0x19))
    {
        Serial.println("Couldn't find MCP9808!");
        //digitalWrite(redLED, HIGH);
        while (1)
            ;
    }
    if (!MCP_B.begin())
    {
        Serial.println("Couldn't find MCP9808!");
        //digitalWrite(redLED, HIGH);
        while (1)
            ;
    }

    if (!ccs.begin())
    {
        Serial.println("Could not find a valid CCS811 sensor, check wiring!");
        while (1)
            ;
    }

    //calibrate CCS811 temperature sensor
    while (!ccs.available())
        ;
    ccs.setTempOffset(ccs.calculateTemperature() - 25.0);

    if (!sgp.begin())
    {
        Serial.println("Could not find a valid SGP30 sensor, check wiring!");
        while (1)
            ;
    }
    if (!sgp.getIAQBaseline(&eCO2_base, &TVOC_base))
    {
        Serial.println("Failed to get baseline readings");
        return;
    }

    //setup ADC
    ads1115.begin();

    //Initalize SD
    if (!SD.begin(4))
    {
        Serial.println("initialization failed!");
        //digitalWrite(redLED, HIGH);
        return;
    }
    Serial.println("initialization done.");

    for (uint8_t i = 0; i < 1000; i++)
    {
        Filename[first0Index] = i / 100 + '0';
        Filename[second0Index] = (i % 100) / 10 + '0';
        Filename[third0Index] = i % 10 + '0';
        if (!SD.exists(Filename))
        {
            File dataFile = SD.open(Filename, FILE_WRITE);
            dataFile.println("millis, Date, Hours, Minutes, Seconds, milliseconds, speed(knots), Latitude, longitude, Satellites, raw X, raw Y, raw Z, H-accelX(G), H-accelY(G), H-accelZ(G), accelX(m/s^2), accelY(m/s^2), accelZ(m/s^2), gyroX(rad/s), gyroY(rad/s), gyroZ(rad/s), magX(uT), magY(uT), magZ(uT), MCP_Temp(C), MCP_Temp_BIO(C), BMP_Temp_Atmo(C), BMP_Temp(C), Atmo_Humidity(%), Humidity_in(%), Presure_Atmo(hPa), Altitude_Atmo(M), Altitude(GPS)(M), CCS_eC02_Gas(ppm), CCS_TV0C_Gas(ppb), CCS_Temp(C), SGP_eC02_Gas(ppm), SGP_TV0C_Gas(ppb), SGP_eC02Gas_basline(hex), SGP_TV0CGas_basline(hex), AIN0_val(0-512)");
            dataFile.close();
            break; // leave the loop!
        }
    }

    Serial.print("Writing to ");
    Serial.println(Filename);

    uint32_t timer = millis();
    int counter = 0; //counter for SGP baseline readings

    while (1)
    {
        char c = GPS.read();

        // if a sentence is received, we can check the checksum, parse it...
        if (GPS.newNMEAreceived())
        {
            if (!GPS.parse(GPS.lastNMEA())) // this also sets the newNMEAreceived() flag to false
                return;                     // we can fail to parse a sentence in which case we should just wait for another
        }

        // if millis() or timer wraps around, we'll just reset it
        if (timer > millis())
            timer = millis();

        // record data String after ColectDelay
        if (millis() - timer > ColectDelay)
        {

            //Open dataFile
            File dataFile = SD.open(Filename, FILE_WRITE);

            //check if dataFile Opend
            if (!dataFile)
            {
                Serial.print("could not make");
                Serial.println(Filename);
                //if SD card did not open Turn on RED led
                sdFailBlink();
                //digitalWrite(GreenLED, HIGH);
            }
            else
            {
                //digitalWrite(redLED, HIGH);
            }

            //blink low twice for no fix
            //blink High once for sucess.
            BlinkLED();

            //Record Time to sd
            RecordTimeDate(dataFile);

            //Record Gps Data to sd
            RecordGPS(dataFile);

            // analog sensor read.
            int xRaw = ReadAxis(xInput);
            int yRaw = ReadAxis(yInput);
            int zRaw = ReadAxis(zInput);

            //analog Acceleration X
            RecordData(dataFile, " RawX: ", xRaw);

            //analog Acceleration X
            RecordData(dataFile, " RawY: ", yRaw);

            //analog Acceleration X
            RecordData(dataFile, " RawZ: ", zRaw);

            // Convert raw values to 'milli-Gs"
            long xScaled = map(xRaw, xRawMin, xRawMax, -1000, 1000);
            long yScaled = map(yRaw, yRawMin, yRawMax, -1000, 1000);
            long zScaled = map(zRaw, zRawMin, zRawMax, -1000, 1000);

            // re-scale to fractional Gs
            float xAccel = xScaled / 1000.0;
            float yAccel = yScaled / 1000.0;
            float zAccel = zScaled / 1000.0;

            //analog sensor maped to
            //Lsm -> ADXL
            //X -> -X
            //Y -> -Z
            //Z ->  Y

            //analog Acceleration X
            RecordData(dataFile, " HighX: ", -xAccel);

            //analog Acceleration X
            RecordData(dataFile, " HighY: ", -zAccel);

            //analog Acceleration X
            RecordData(dataFile, " HighZ: ", yAccel);

            //90dof Sensor Read
            lsm.read();
            sensors_event_t a, m, g, temp;
            lsm.getEvent(&a, &m, &g, &temp);

            //acceleration X
            RecordData(dataFile, " AX: ", a.acceleration.x);

            //acceleration Y
            RecordData(dataFile, " AY: ", a.acceleration.y);

            //acceleration Z
            RecordData(dataFile, " AZ: ", a.acceleration.z);

            //Gyro X
            RecordData(dataFile, " GX: ", g.gyro.x);

            //Gyro Y
            RecordData(dataFile, " GY: ", g.gyro.y);

            //Gyro Z
            RecordData(dataFile, " GZ: ", g.gyro.z);

            //Mag x
            RecordData(dataFile, " MX: ", m.magnetic.x);

            //Mag y
            RecordData(dataFile, " MY: ", m.magnetic.y);

            //Mag z
            RecordData(dataFile, " MZ: ", m.magnetic.z);

            //Main temp
            RecordData(dataFile, " T0: ", MCP_A.readTempC());

            //Bio temp
            RecordData(dataFile, " T1: ", MCP_B.readTempC());

            //atmospheric temp
            RecordData(dataFile, " T2: ", bmeA.readTemperature());

            //outside temp
            RecordData(dataFile, " T3: ", bmeB.readTemperature());

            //atmospheric humidity
            RecordData(dataFile, " H1: ", bmeA.readHumidity());
            //humidity
            RecordData(dataFile, " H2: ", bmeB.readHumidity());

            //Presure
            RecordData(dataFile, " Hpa: ", bmeA.readPressure() / 100.0F);

            //Altitude
            RecordData(dataFile, " Altm: ", bmeA.readAltitude(SEALEVELPRESSURE_HPA));

            RecordData(dataFile, " Altm GPS: ", GPS.altitude);

            //Gas Sensor
            if (ccs.available() && !ccs.readData())
            {
                RecordData(dataFile, " eC02 Gas1: ", ccs.geteCO2());
                RecordData(dataFile, " TV0C Gas: ", ccs.getTVOC());
                RecordData(dataFile, " CCS Temp: ", ccs.calculateTemperature());
            }

            //SGP Sensor
            if (!sgp.IAQmeasure())
            {
                Serial.println("SGP Measurement failed");
                return;
            }
            RecordData(dataFile, " eC02 Gas2: ", sgp.eCO2);
            RecordData(dataFile, " TV0C Gas2: ", sgp.TVOC);
            counter++;
            if (counter == 30)
            {
                counter = 0;
                if (!sgp.getIAQBaseline(&eCO2_base, &TVOC_base))
                {
                    Serial.println("Failed to get baseline readings");
                    return;
                }
            }
            RecordData(dataFile, " eC02 baseline: ", eCO2_base);
            RecordData(dataFile, " TVOC baseline: ", TVOC_base);

            //Single Ended Conversion ADC
            RecordData(dataFile, " AIN0: ", ads1115.readADC_SingleEnded(0));

            //new line
            Serial.println();
            dataFile.println();
            dataFile.close();
            timer = millis(); // reset the timer
        }
    }
}

void RecordGPS(File dataFile) {

  //GPS data
  Serial.print(" knots ");
  Serial.print(GPS.speed);
  Serial.print(",");
  Serial.print("Location degrees: ");
  Serial.print(GPS.latitudeDegrees, 4);
  Serial.print(", ");
  Serial.print(GPS.longitudeDegrees, 4);
  Serial.print(", ");
  Serial.print(" sat ");
  Serial.print((int)GPS.satellites);
  Serial.print(",");
  dataFile.print(GPS.speed);
  dataFile.print(",");
  dataFile.print(GPS.latitudeDegrees, 4);
  dataFile.print(", ");
  dataFile.print(GPS.longitudeDegrees, 4);
  dataFile.print(", ");
  dataFile.print((int)GPS.satellites);
  dataFile.print(",");

}

void RecordTimeDate(File dataFile) {

  int gps_hour = GPS.hour - 7;
  int gps_day = GPS.day;
  if (gps_hour <= 0) {
    gps_hour = 24 + gps_hour;
    gps_day --;
  }
  if (gps_hour > 12) {
    gps_hour -= 12;
  }

  Serial.print("millis ");
  Serial.print(millis(), DEC);
  Serial.print(" Date/Time ");
  Serial.print(GPS.month, DEC);
  Serial.print("/");
  Serial.print(gps_day, DEC);
  Serial.print(", ");
  Serial.print(gps_hour);
  Serial.print(":");
  Serial.print(GPS.minute);
  Serial.print(":");
  Serial.print(GPS.seconds);
  Serial.print(":");
  Serial.print(GPS.milliseconds);
  Serial.print(",");

  dataFile.print(millis(), DEC);
  dataFile.print(",");
  dataFile.print(GPS.month, DEC);
  dataFile.print("/");
  dataFile.print(gps_day, DEC);
  dataFile.print(",");
  dataFile.print(gps_hour);
  dataFile.print(",");
  dataFile.print(GPS.minute);
  dataFile.print(",");
  dataFile.print(GPS.seconds);
  dataFile.print(",");
  dataFile.print(GPS.milliseconds);
  dataFile.print(",");



}



void BlinkLED() {

  if (!GPS.fix) {
    Serial.println("No Fix");

    //digitalWrite(redLED, HIGH);
    digitalWrite(greenLED, LOW);
    delay(100);
    digitalWrite(greenLED, HIGH);
    delay(50);
    digitalWrite(greenLED, LOW);
    delay(100);
    digitalWrite(greenLED, HIGH);

  }
  else {
    //digitalWrite(redLED, HIGH);
    digitalWrite(greenLED, LOW);
    delay(100);
    digitalWrite(greenLED, HIGH);
  }
}

void sdFailBlink() {

  digitalWrite(greenLED, LOW);
  delay(25);
  digitalWrite(greenLED, HIGH);
  delay(25);
  digitalWrite(greenLED, LOW);
  delay(24);
  digitalWrite(greenLED, HIGH);
  delay(25);
  digitalWrite(greenLED, LOW);
  delay(25);
  digitalWrite(greenLED, HIGH);

}



//
// Read "sampleSize" samples and report the average
//
int ReadAxis(int axisPin)
{
  long reading = 0;
  analogRead(axisPin);
  delay(1);
  for (int i = 0; i < sampleSize; i++)
  {
    reading += analogRead(axisPin);
  }
  return reading / sampleSize;
}


void RecordData(File dataFile, char* Dataname, float data) {

  Serial.print(Dataname);
  Serial.print(data, 4);
  Serial.print(",");
  dataFile.print(data, 4);
  dataFile.print(",");

}
